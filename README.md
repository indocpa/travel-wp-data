Wordpress data untuk offer Travel
===================

Berikut berisi beberapa dump file untuk offer Travel: Agoda
File dengan ekstensi **.txt** adalah daftar hotel_id untuk masing-masing hotel berdasar bintangnya. Masing-masing file berisi 10.000 data.

File dengan ekstensi **.xml** adalah wordpress export file. Silakan diimpor dari Wordpress.

File dengan ekstensi **.sql** adalah data dump dari MySQL. Untuk username Wordpress adalah: **indocpa** dan password adalah: **indocpa**. Setelah impor data sql, ubah option_value dari option_name: **siteurl** dan **home** dari 'http://travelweb.dev' sesuai dengan domain Anda

